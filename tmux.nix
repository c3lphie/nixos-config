{config, ...}:
{
  programs.tmux = {
    enable = true;
    shortcut = "a";
    keyMode = "vi";
    baseIndex = 1;
    extraConfig = ''
set -g mouse on
unbind v
unbind h
unbind %
unbind '"'

bind v split-window -h -c "#{pane_current_path}"
bind b split-window -v -c "#{pane_current_path}"


'';
  };
}
