{config, pkgs, ...}:
{
  users.users = {
    c3lphie = {
      description = "Main user for my machines";
      isNormalUser = true;
      createHome = true;
      home = "/home/c3lphie";
      shell = pkgs.bash;
      extraGroups = [
        "wheel"
        "plugdev"
        "networkmanager"
        "adbusers"
        "libvirtd"
      ];
      openssh.authorizedKeys.keyFiles = [
        ./keys/c3lphie_smartcard_rsa.pub
      ];
    };
  };
}
