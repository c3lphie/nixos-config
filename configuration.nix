{config, pkgs, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./machines/laptop.nix
    ./users.nix
    ./security.nix
    ./tmux.nix
  ];

  time.timeZone = "Europe/Copenhagen";

  console.keyMap = "dk";
  
  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    gnupg1
    pass
    unzip
  ];

  programs.bash = {
    shellInit = ''
export PROMPT_DIRTRIM=2
'';
  };


  system.stateVersion = "21.11";
}
