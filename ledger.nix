{
  services.udev.extraRules = ''
  # Nano X
  SUBSYSTEMS=="usb", ATTRS{idVendor}=="2c97", ATTRS{idProduct}=="0004|4000|4001|4002|4003|4004|4005|4006|4007|4008|4009|400a|400b|400c|400d|400e|400f|4010|4011|4012|4013|4014|4015|4016|4017|4018|4019|401a|401b|401c|401d|401e|401f", MODE="0660", GROUP="plugdev",TAG+="uaccess", TAG+="udev-acl"
  ''
  ;
}
