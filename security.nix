{ config, pkgs, lib, ...}:
# let
#   lockscript = import ./tools/lock;
# in
{
  /******************
   Yubikey integration
  *******************/
  # services.udev.packages = [
  #   pkgs.yubikey-personalization
  #   (pkgs.writeTextFile {
  #     name = "yubikey-lockout";
  #     text = ''ACTION=="remove", ENV{ID_BUS}=="usb", ENV{ID_MODEL_ID}=="0407", ENV{ID_VENDOR_ID}=="1050", ENV{ID_SERIAL_SHORT}=="Yubico_YubiKey_OTP+FIDO+CCID", RUN+="${lockscript}/bin/lockscript"'';
  #     destination = "/etc/udev/rules.d/50-yubikey-lockout.rules";
  #  })
    
  # ];
  security.pam.yubico = {
    enable = true;
    # debug = true;
    mode = "challenge-response";
  };
  # environment.systemPackages = [
  #   lockscript
  # ];

  # Use GPG for ssh
  environment.shellInit = ''
    export GPG_TTY="$(tty)"
    gpg-connect-agent /bye
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
  '';
  services.pcscd.enable = true; # Enable smartcard to handle yubikey's smartcard functionality
  # Use GPG agent instead of SSH agent
  programs = {
    ssh.startAgent = false; 
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
}
