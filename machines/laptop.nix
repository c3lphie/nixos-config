{ config, pkgs, ... }:
let
  nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
    export __NV_PRIME_RENDER_OFFLOAD=1
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
    export __GLX_VENDOR_LIBRARY_NAME=nvidia
    export __VK_LAYER_NV_optimus=NVIDIA_only
    export CUDA_PATH=${pkgs.cudatoolkit}
    exec "$@"
  '';
  my-rofi-pass = (pkgs.writeScriptBin "rofi-pass"
    (builtins.readFile ./laptop/rofi_scripts/rofi-pass)).overrideAttrs (old: {
      buildCommand = ''
        ${old.buildCommand}
        patchShebangs $out'';
    });
  rofi-screenlayout = (pkgs.writeScriptBin "rofi-screenlayout"
    (builtins.readFile ./laptop/rofi_scripts/rofi-screenlayout)).overrideAttrs (old: {
      buildCommand = ''
        ${old.buildCommand}
        patchShebangs $out'';
    });
in {
  imports = [
    "${builtins.fetchGit{ url = "https://github.com/NixOS/nixos-hardware.git";}}/lenovo/thinkpad/p53"
    ../ledger.nix
  ];
  # General machine settings
  nixpkgs.config.allowUnfree = true; # Allow nvidia driver
  virtualisation.libvirtd.enable = true;
  virtualisation.docker.enable = true;
  services.xserver = {
    enable = true;
    videoDrivers = [
      "nvidia"
    ];
    layout = "dk";
    displayManager.startx.enable = true;
    desktopManager.xterm.enable = false;
    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [ rofi i3status i3lock ];
    };
    libinput.enable = true;
  };
  services.emacs = { enable = true; };
  services.gnome.gnome-keyring.enable = true;
  services.hardware.bolt.enable = true;

  programs.adb.enable = true;

  hardware = {
    opengl.enable = true;
    bluetooth.enable = true;
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
    };
    nvidia.modesetting.enable = true;
    nvidia.prime = {
      offload.enable = true;
      intelBusId = "PCI:0:2:0";
      nvidiaBusId = "PCI:1:0:0";
    };
  };

  # Packages
  environment.systemPackages = with pkgs; [
    emacs
    sqlite
    plantuml
    python39Packages.python-lsp-server
    rust-analyzer
    clang-tools
    texlive.combined.scheme-full
    auctex
    zip
    poppler
    alacritty
    wireguard-tools
    zathura

    libnotify
    dunst
    qrencode
    feh

    kicad

    # My scripts
    my-rofi-pass
    rofi-screenlayout

    # =======

    monero-gui
    ledger-live-desktop

    nheko
    xclip
    discord
    file
    tmux
    firefox
    flameshot

    spotify

    virt-manager

    man-pages
    man-pages-posix

    pulsemixer

    cudatoolkit
    
    nvidia-offload

    # Hacking tools
    hashcat
    hashcat-utils
    hcxtools
    thc-hydra
    sqlmap
    metasploit
    burpsuite
    jadx
    wireshark
    gobuster
    openvpn
    nmap
    ghidra-bin
  ];

  # Nur
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball
      "https://github.com/nix-community/NUR/archive/master.tar.gz") {
        inherit pkgs;
      };
  };

  # Machine user conf

  # Networking
  networking = {
    hostName = "laptop";
    networkmanager = {
      enable = true;

    };
    hostFiles = [
     ../hosts
    ];
    nameservers = [ "192.168.1.33" ];
    firewall = {
      # allowedTCPPorts = [ 8000 ];
      # allowedUDPPorts = [ 2024 ];
    };
  };
  security.pki.certificateFiles =
    [ "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt" ../certs/aau_net_ca.crt ];
  # Fonts
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
    liberation_ttf
  ];

  # Taking out the trash
  nix.gc = {
    automatic = true;
    dates = "weekly";
  };

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  boot.loader.efi.canTouchEfiVariables = true;
  boot.binfmt.emulatedSystems  = ["aarch64-linux"];
  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "nodev";
    efiSupport = true;
    enableCryptodisk = true;
  };
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.initrd.luks.devices = {
    encp1 = {
      device = "/dev/disk/by-uuid/7f842e0e-e32f-49d0-937e-52d36ca0efc9";
      allowDiscards = true;
      preLVM = true;
    };
    encp2 = {
      device = "/dev/disk/by-uuid/0caed244-67af-428f-a8d1-a7caa3c9e8c1";
      allowDiscards = true;
      preLVM = true;
    };
  };
  boot.blacklistedKernelModules = [
    "nouveau"
    "rivafb"
    "nvidiafb"
    "rivatv"
    "nv"
    "uvcvideo"
  ];
  boot.extraModulePackages = [
    config.boot.kernelPackages.nvidia_x11
  ];
}
