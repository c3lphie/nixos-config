{ ... }: {
  imports = [
    ./hardware-configuration.nix
    

  ];

  services.minecraft-server = {
    enable = true;
    eula = true;
    openFirewall = true;
  };

  
  boot.cleanTmpDir = true;
  zramSwap.enable = true;
  networking.hostName = "ubuntu-2gb-fsn1-1";
}
